import { getWorkForm, renderList } from "./controller.js";

let todoList = [];
let BASE_URL = "https://630092419a1035c7f8f46aac.mockapi.io";

let renderService = () => {
  axios({
    url: `${BASE_URL}/todolist`,
    method: "GET",
  })
    .then(function (res) {
      todoList = res.data;
      renderList(todoList);
    })
    .catch(function (err) {
      console.log(err);
    });
};
renderService();
//  xóa công việc
let remove = (id) => {
  axios({
    url: `${BASE_URL}/todolist/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      renderService();
      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
};
window.remove = remove;
// thêm mới công việc
let addTask = () => {
  let dataForm = getWorkForm();

  axios({
    url: `${BASE_URL}/todolist`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      renderService();
      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
};
window.addTask = addTask;
