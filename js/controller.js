export let renderList = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let content = `
        <li id="${item.id}">
                    ${item.work}
                    <div class="buttons">
                      <button onclick="remove('${item.id}')" class="remove">
                        <i class="fa fa-trash-alt"></i>
                      </button>
                      <button onclick="done('${item.work}','${item.id}')" class="complete">
                        <i class="fa fa-check-circle"></i>
                      </button>
                    </div>
                  </li>
    
          `;
    contentHTML += content;
  });
  document.getElementById("todo").innerHTML = contentHTML;
};
export let getWorkForm = () => {
  let work = document.getElementById("newTask").value.trim();
  return {
    work: work,
  };
};
const listDone = [];
let done = (work, id) => {
  document.getElementById(id).style.display = "none";

  listDone.push(work);
  console.log("listDone: ", listDone);
  let contentDone = "";
  for (let i = 0; i < listDone.length; i++) {
    let content = `
 <li> ${listDone[i]}
  <div class="buttons">
    <button onclick="re('${i}','${id}')" class="complete">
      <i class="fa fa-check-circle"></i>
    </button>
  </div>
</li>`;
    contentDone += content;
    document.getElementById("completed").innerHTML = contentDone;
  }
};
window.done = done;

let re = (i, id) => {
  listDone.splice(i, 1);
  document.getElementById(id).style.display = "inline-block";
};
window.re = re;
